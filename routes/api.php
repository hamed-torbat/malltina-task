<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api'], function (){

    Route::post('login', 'AuthenticationController@login');
//    Route::get('logout', 'AuthenticationController@logout');

//    Route::resource('order', 'OrderController');
    Route::group(['prefix' => 'order', 'middleware' => 'auth:api'], function (){
        Route::get('menu', 'OrderController@menu');
        Route::post('register', 'OrderController@register');
        Route::put('status', 'OrderController@changeOrderStatus');
    });


});
