<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::view('/', 'panel.login'); // view - redirect to "login page"

Route::group(['prefix' => 'panel', 'namespace' => 'Panel'], function (){

    // login & logout...
    Route::view('login', 'panel.login'); //view
    Route::post('login', 'AuthenticationController@login');
    Route::get('logout', 'AuthenticationController@logout');

    // Panel Admin...
    Route::group(['prefix' => 'admin', 'middleware' => ['guard','preloader']], function (){

        // product...
        Route::group(['prefix' => 'product', 'as' => 'product'], function (){

            Route::get('/', 'productController@product');
            Route::post('status', 'productController@changeOrderStatusAjax');

        });

    });

});
