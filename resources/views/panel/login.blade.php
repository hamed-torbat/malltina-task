<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title>ورود | login</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- BEGIN GLOBAL MANDATORY STYLES -->
{{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>--}}
{{--<link href="{{asset('template-assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>--}}
{{--<link href="{{asset('template-assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>--}}
<link href="{{asset('template-assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
{{--<link href="{{asset('template-assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>--}}
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('template-assets/admin/pages/css/lock-rtl.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{asset('template-assets/global/css/components-md-rtl.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
{{--<link href="{{asset('template-assets/global/css/plugins-md-rtl.css')}}" rel="stylesheet" type="text/css"/>--}}
{{--<link href="{{asset('template-assets/admin/layout/css/layout-rtl.css')}}" rel="stylesheet" type="text/css"/>--}}
{{--<link href="{{asset('template-assets/admin/layout/css/themes/darkblue-rtl.css')}}" rel="stylesheet" type="text/css" id="style_color"/>--}}
{{--<link href="{{asset('template-assets/admin/layout/css/custom-rtl.css')}}" rel="stylesheet" type="text/css"/>--}}
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<div class="page-lock">
	<div class="page-logo">
		{{--<a class="brand" href="index.html">--}}
		{{--<img src="{{asset('images/general/logo-panel.jpg')}}" alt="logo"/>--}}
		{{--</a>--}}
		<h3 style="color: #0d809d;">
			{{--به <a href="#" style="color: #1fb4da">ویلایپی</a> خوش آمدید--}}
			<br>
		</h3>

	</div>
	<div class="page-body">
		<div class="lock-head" style="color: #1b9cbd;font-size: 20px;">
			 ورود به سیستم
		</div>
		<div class="lock-body">
			<div class="pull-left lock-avatar-block">
{{--				<img src="{{asset('template-assets/admin/pages/media/profile/photo3.jpg')}}" class="lock-avatar">--}}
				<img src="{{asset('assets/images/general/logo-login.jpg')}}" class="lock-avatar"/>
			</div>


			<form class="lock-form pull-left" action="{{url('panel/login')}}" method="post">
				{{ csrf_field() }}
				<div class="form-group">
					<input type="text" name="email" class="form-control placeholder-no-fix"  placeholder="ایمیل"/>
				</div>
                <div class="form-group">
                    <input type="text" name="password" class="form-control placeholder-no-fix"  placeholder="رمز عبور"/>
                </div>
				<div class="form-actions">
					<button type="submit" class="btn btn-success uppercase">ارسال</button>
				</div>
			</form>


		</div>
		<div class="lock-bottom">

            @if($error = Session::get('error'))
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $error }}</strong>
                    </div>
                </div>
            @endif

			{{--<a href="">Not Amanda Smith?</a>--}}
			{{--<span style="color: #323d4b;">villaipi</span>--}}
{{--			<span style="color: #1b9cbd;">بعد از ارسال شماره موبایل، "کد تایید" برای شما پیامک خواهد شد.</span>--}}
		</div>
	</div>
	{{--<div class="page-footer-custom">--}}
		{{--2014 © Metronic. Admin Dashboard Template--}}
	{{--</div>--}}

</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{asset('template-assets/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('template-assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('template-assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('template-assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('template-assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('template-assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('template-assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('template-assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>--}}
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
{{--<script src="{{asset('template-assets/global/plugins/backstretch/jquery.backstretch.min.js')}}" type="text/javascript"></script>--}}
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{asset('template-assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('template-assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('template-assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>

<script type="text/javascript">

	// $('button:submit').on('click' , function (e) {
    //     e.preventDefault()
    //
	// 	var mobile = $('input[name=mobile]').val();
    //     $.ajax({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         url:'login',
    //         data: {mobile: mobile},
    //         type: 'POST',
    //         dataType: 'JSON',
    //         success : function(res){
    //             // location.reload();
	// 			if ( res ){
	// 				$('form').submit();
	// 				return;
	// 			}
    //
	// 			$('.error').text('شماره موبایل معتبر نیست');
	// 			return
    //
    //         },
    //
    //         // beforeSend: function(){
    //         //     $('.preloader').show()
    //         // },
    //
    //         // complete: function(){
    //         //     $('.preloader').hide()
    //         // }
    //
    //     })
    // })

</script>

<script>
jQuery(document).ready(function() {
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init();
});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
