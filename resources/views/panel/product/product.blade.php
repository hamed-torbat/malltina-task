@extends('layouts.panelLayout')

@section('title' , 'سفارش')

@section('css')

    <meta http-equiv="refresh" content="300"> {{-- auto refresh after 300second (5min) --}}

    <style>

    </style>
@endsection

@section('content')

    @if ($message = Session::get('successful'))
        <div class="col-md-12">
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        </div>
    @elseif($error = Session::get('error'))
        <div class="col-md-12">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $error }}</strong>
            </div>
        </div>
    @endif


    <div class="col-md-12"> <br> </div>

    <div class="col-md-12">

        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-basket"></i> سفارش
                </div>
            </div>
            <div class="portlet-body">
                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">

                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid" aria-describedby="sample_1_info">
                            <thead>
                            <tr role="row">
                                <th>#</th>
                                <th>user</th>
                                <th>consume location</th>
                                <th>order item</th>
                                <th>total price</th>
                                <th>status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $key => $order)
                                <tr>
                                    <td>
                                        {{++$key}}
                                    </td>
                                    <td>
                                        {{optional($order->user)->full_name }}
                                    </td>
                                    <td>
                                        {{$order->consume_location}}
                                    </td>
                                    <td>
                                        @foreach($order->order_item as $key => $order_item)
                                            <span>{{optional($order_item->product)->title}}</span>
                                             -
                                            <span>{{optional($order_item->product_option)->title}}</span>
                                            <br>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{$order->total_price}}
                                    </td>
                                    <td>

                                        {{-- change status with ajax request --}}
                                        <select name="status" data-id="{{$order->id}}" class="form-control input-sm change-status" id="form_control_1" style="padding: 0px;height: 26px;">
                                            @foreach($status as $value)
                                                @if($value == $order->status)
                                                    <option value="{{$value}}" selected>{{$value}}</option>
                                                @else
                                                    <option value="{{$value}}">{{$value}}</option>
                                                @endif
                                            @endforeach

                                        </select>

                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            {{ $orders->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>




@endsection

@section('js')
    <script type="text/javascript">

        $('select.change-status').on('change', function () {


            confirm("Do you want to continue?");

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'product/status',
                data: {id: $('select[name=status]').data('id'), status: $('select[name=status]').val()},
                type: 'POST',
                dataType: 'JSON',
                success : function(res){

                    if(res == 'false'){
                        alert("fail");
                    }

                    alert("successful");

                },

                // beforeSend: function(){
                //     $('.preloader').show()
                // },
                //
                // complete: function(){
                //     $('.preloader').hide()
                // }

            })


            {{--var status = "{{url('panel/admin/product')}}";--}}
            {{--window.location.href = status;--}}
        })


    </script>
@stop
