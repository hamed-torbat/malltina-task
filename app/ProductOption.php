<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{

    protected $fillable = [
        'title', 'parent_id'
    ];


    // parent option
    public function parent(){
        return $this->belongsTo(ProductOption::class, 'parent_id');
    }

    // children option
    public function children(){
        return $this->hasMany(ProductOption::class, 'parent_id');
    }


    public function product(){
        return $this->hasMany(Product::class);
    }

    public function order_item(){
        return $this->hasMany(OrderItem::class);
    }


}
