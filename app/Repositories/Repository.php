<?php


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class Repository
{

    protected $model;

    // Constructor to bind model to repository...
    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    // create a new record in the database...
    public function create(array $data)
    {
        return $this->model->create($data);
    }


    // remove record from the database...
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

}
