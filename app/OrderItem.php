<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $fillable = [
        'order_id', 'product_id', 'product_option_id', 'count', 'total_price'
    ];


//    protected $appends = [
//        'order_product', 'order_product_option'
//    ];
//    public function getOrderProductAttribute($value){
//        $product = $this->product()->first();
//        if ( $product ){
//            return $product->title;
//        }
//        return $product;
//    }
//    public function getOrderProductOptionAttribute($value){
//        $product_option = $this->product_option()->first();
//        if ( $product_option ){
//            return $product_option->title;
//        }
//        return $product_option;
//    }


    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function product_option(){
        return $this->belongsTo(ProductOption::class);
    }

}
