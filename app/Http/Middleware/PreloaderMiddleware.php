<?php

namespace App\Http\Middleware;

use App\Permission;
use App\PermissionRole;
use App\RoleUser;
use Auth;
use View;
use Closure;

class PreloaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $login_user_info = Auth::user();

//        group route name
        $route_name = Request()->route()->getName();


//        $segment1 = $request->segment(1);
//        $segment2 = $request->segment(2);
//        $path = $request->path();




        View::share(['route_name' => $route_name, 'login_user_info' => $login_user_info]);

        return $next($request);


    }
}
