<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderItem;
use App\Product;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class OrderController extends Controller
{


    public function menu(Request $request){
//        if (request()->isMethod('get')) {

            $user = Auth::user();

            $products = Product::with('product_option.children')->get();
            $user_orders = Order::where('user_id', $user->id)->with('order_item.product')->select('id', 'total_price', 'status')->get();

            return response()->json([
                "success" => true,
                "data"    => [
                    "message"     => 'successful',
                    "menu"        => $products,
                    "user_orders" => $user_orders,
                ],
            ], 200);

//        }
    }


    public function register(Request $request)
    {

        $credentials = $request->only('consume_location', 'order_item');
        $validator = Validator::make($credentials, [
            'consume_location'  => 'required|string',
            'order_item'        => 'required|array',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "data"    => [
                    "message" => $validator->errors()->all(),
                ],
            ], 422);
        }

        $user = Auth::user();

        // register new order...
        $repository = new Repository(new Order());
        $order = $repository->create([
            "user_id"          => $user->id,
            "consume_location" => $request->consume_location,
            "status"           => 'waiting',
        ]);


        // register order_items of the order...
        $order_total_price = 0;

        $repository = new Repository(new OrderItem());
        foreach (request()->order_item as $order_item){

            $repository->create([
                "order_id"          => $order->id,
                "product_id"        => $order_item['product_id'],
                "product_option_id" => $order_item['product_option_id'],
                "count"             => $order_item['count'],
                "total_price"       => $order_item['total_price'],
            ]);

            // calculates total_price of the order...
            $order_total_price = $order_total_price + $order_item['total_price'];

        }

        // Updated total_price of the order...
        $order->total_price = $order_total_price;
        $order->save();

        return response()->json([
            "success" => true,
            "data"    => [
                "message"     => 'successful',
//                    "order_item"  => $order->with('order_item')->get(),
            ],
        ], 200);

    }


    public function changeOrderStatus(Request $request)
    {

        $credentials = $request->only('id', 'status');
        $validator = Validator::make($credentials, [
            'id'     => 'required|integer|exists:orders',
            'status' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "data"    => [
                    "message" => $validator->errors()->all(),
                ],
            ], 422);
        }

        $order = Order::where(['id' => $request->id, 'status' => 'waiting'])->first();
        if ( !$order ) {
            return response()->json([
                "success" => false,
                "data"    => [
                    "message" => 'order not found',
                ],
            ], 404);
        }

        $order->status = $request->status;
        $order->save();

        return response()->json([
            "success" => true,
            "data"    => [
                "message" => 'successful',
                "order"   => $order,
            ],
        ], 200);

    }


}
