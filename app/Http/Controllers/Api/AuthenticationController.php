<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;
use Validator;

class AuthenticationController extends Controller
{

    public function login(Request $request){

//        if ($request->isMethod('post')){

            $credentials = $request->only('email','password');
            $validator = Validator::make($credentials , [
                'email'    => 'required|email',
                'password' => 'required|string'
            ]);
            if ($validator->fails()){
                return response()->json([
                    "success" => false,
                    "data"    => [
                        "message" => $validator->errors()->all(),
                    ],
                ],422);
            }


            if(!Auth::attempt($credentials)){
                return response()->json([
                    "success" => false,
                    "data"    => [
                        "message" => "Unauthorized",
                    ],
                ],401);
            }

            $user = Auth::user();
            $secret = DB::table('oauth_clients')->find(2)->secret;
            $tokenResult = $user->createToken($secret);

            return response()->json([
                "success" => true,
                "data"    => [
                    "message"      => 'Authorized',
                    'access_token' => $tokenResult->accessToken,
                    'token_type'   => 'Bearer',
                    'expires_at'   => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString()
                ],
            ],200);

//        }

    }


//    public function logout(Request $request) {
//
//        $value = $request->bearerToken();
//        $tokenId = (new Parser())->parse($value)->getHeader('jti');
//        $token = Token::find($tokenId);
//        $token->revoke();
//
//        return response()->json([
//            "success" => true,
//            "data"    => [
//                "message" => 'successful',
//                "token"   => $token,
//            ],
//        ],200);
//
//    }




}
