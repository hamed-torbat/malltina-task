<?php

namespace App\Http\Controllers\Panel;

use App\Classes\HelperFunc;
use App\Mail\ChangeOrderStatus;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;

class productController extends Controller
{


    public function product(){

        $orders = Order::with('user', 'order_item')->paginate(10);
        $status = HelperFunc::getEnumColumnValues('orders', 'status');

        return view('panel.product.product', compact('orders', 'status'));

    }

    public function changeOrderStatusAjax(Request $request)
    {

        $order = Order::where(['id' => $request->id])->first();
        if ( !$order ) {
            return 'false';
        }

        $order->status = $request->status;
        $order->save();


        Mail::to( optional($order->user)->email )->send(new ChangeOrderStatus());

        return 'true';

    }


}
