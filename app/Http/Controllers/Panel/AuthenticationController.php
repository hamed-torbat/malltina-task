<?php

namespace App\Http\Controllers\Panel;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthenticationController extends Controller
{

    public function login(Request $request){

        if ($request->isMethod('post')){

            $credentials = $request->only('email','password');
            $validator = Validator::make($credentials , [
                'email'    => 'required|email',
                'password' => 'required|string'
            ]);
            if ($validator->fails()){
                if ($validator->fails()){
                    return back()->with('errors' , $validator->errors());
                }
            }

            if(!Auth::attempt($credentials)){
                return back()->with('error' , "Unauthorized");
            }


            return redirect( 'panel/admin/product' );

        }

    }


    public function logout(){
        Auth::logout();
        return redirect( 'panel/login' );
    }


}
