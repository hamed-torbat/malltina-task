<?php


namespace App\Classes;

use Illuminate\Support\Facades\DB;

class HelperFunc
{


    public static function getEnumColumnValues($table, $column) {

        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type ;

        preg_match('/^enum\((.*)\)$/', $type, $matches);

        $enum_values = [];
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum_values[] = $v;
        }
        return $enum_values;
    }


}
