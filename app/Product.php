<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'title', 'price', 'product_option_id'
    ];


    public function product_option(){
        return $this->belongsTo(ProductOption::class);
    }

    public function order_item(){
        return $this->hasMany(Product::class);
    }

}
