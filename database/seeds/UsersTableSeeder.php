<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->insert([
//            'full_name' => Str::random(10),
//            'email' => Str::random(10).'@gmail.com',
//            'password' => bcrypt(123456),
//        ]);

        \App\User::create([
            'full_name' => 'حامد تربت',
            'email' => 'hmd.torbat@gmail.com',
            'password' => bcrypt(123456),
            'role_id' => 1,
        ]);
        \App\User::create([
            'full_name' => 'کوروش شریفی',
            'email' => 'sharifi@gmail.com',
            'password' => bcrypt(123456),
            'role_id' => 2,
        ]);

    }
}
