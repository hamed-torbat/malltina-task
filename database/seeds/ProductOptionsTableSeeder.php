<?php

use Illuminate\Database\Seeder;

class ProductOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        // parent...
        \App\ProductOption::create([
            'id' => 1,
            'title' => 'milk',
        ]);
        \App\ProductOption::create([
            'id' => 2,
            'title' => 'size',
        ]);
        \App\ProductOption::create([
            'id' => 3,
            'title' => 'shot',
        ]);
        \App\ProductOption::create([
            'id' => 4,
            'title' => 'kind',
        ]);


        // child...
        // milk
        \App\ProductOption::create([
            'title' => 'skim',
            'parent_id' => 1,
        ]);
        \App\ProductOption::create([
            'title' => 'semi',
            'parent_id' => 1,
        ]);
        \App\ProductOption::create([
            'title' => 'whole ',
            'parent_id' => 1,
        ]);
        // Size
        \App\ProductOption::create([
            'title' => 'small',
            'parent_id' => 2,
        ]);
        \App\ProductOption::create([
            'title' => 'medium',
            'parent_id' => 2,
        ]);
        \App\ProductOption::create([
            'title' => 'large',
            'parent_id' => 2,
        ]);
        // Shots
        \App\ProductOption::create([
            'title' => 'single',
            'parent_id' => 3,
        ]);
        \App\ProductOption::create([
            'title' => 'double',
            'parent_id' => 3,
        ]);
        \App\ProductOption::create([
            'title' => 'triple',
            'parent_id' => 3,
        ]);
        // kind
        \App\ProductOption::create([
            'title' => 'chocolate_chip',
            'parent_id' => 4,
        ]);
        \App\ProductOption::create([
            'title' => 'ginger',
            'parent_id' => 4,
        ]);


    }
}
