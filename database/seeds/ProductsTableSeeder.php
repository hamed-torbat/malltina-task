<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::create([
            'title' => 'Latte',
            'price' => 1000,
            'product_option_id' => 1,
        ]);

        \App\Product::create([
            'title' => 'Cappuccino',
            'price' => 1500,
            'product_option_id' => 2,
        ]);

        \App\Product::create([
            'title' => 'Espresso',
            'price' => 2000,
            'product_option_id' => 3,
        ]);

        \App\Product::create([
            'title' => 'Tea',
            'price' => 2500,
//            'product_option_id' => ,
        ]);

        \App\Product::create([
            'title' => 'Chocolate',
            'price' => 3000,
            'product_option_id' => 2,
        ]);

        \App\Product::create([
            'title' => 'Cookie',
            'price' => 3500,
            'product_option_id' => 4,
        ]);


    }
}
