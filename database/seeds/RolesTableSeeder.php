<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'id' => 1,
            'title' => 'super_admin',
            'landing_page' => 'user',
        ]);
        \App\Role::create([
            'id' => 2,
            'title' => 'admin',
            'landing_page' => 'user',
        ]);
        \App\Role::create([
            'id' => 3,
            'title' => 'user',
//            'landing_page' => '',
        ]);

    }
}
